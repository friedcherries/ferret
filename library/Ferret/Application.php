<?php

namespace Ferret;

class Application {
  protected $view;
  protected $configs;
  protected $dbh;
  protected $pluginResults;
  protected $controller;
  protected $action;
  protected $variables;

  public function __construct($configFile) {

    if (stream_resolve_include_path($configFile)) {
      $this->configs = include $configFile;
    } else {
      throw new Exception ("Required config file not found!");
    }

    if (is_array($this->configs)) {
      if (array_key_exists('pdo.dsn', $this->configs) &&
        array_key_exists('pdo.user', $this->configs) &&
        array_key_exists('pdo.pass', $this->configs)) {
        try {
          $this->dbh = new \PDO($this->configs['pdo.dsn'], $this->configs['pdo.user'], $this->configs['pdo.pass']);  
        } catch (\PDOException $e) {
          die($e->getMessage()); /** TODO: Fix this later!!! **/
        }
      }

      // Implement our autoloader if the user wants it
      if (array_key_exists('autoloader', $this->configs) && $this->configs['autoloader'] === true) {
        spl_autoload_register(function ($className) {
            $className = (string) str_replace('\\', DIRECTORY_SEPARATOR, $className);
            require_once($className . '.php');
          });
      }

      // If they provided a timezone then set it
      if (!empty($this->configs['timezone'])) {
         date_default_timezone_set($this->configs['timezone']);
      }
    }
  }

  protected function executePlugins() {
    $results = null;
    if (array_key_exists('execution.plugins', $this->configs)) {
      if (is_array($this->configs['execution.plugins'])) {
        foreach ($this->configs['execution.plugins'] as $pName) {
          $p = new $pName($this->dbh);
          $results[$pName] = $p->execute();
        }
        $this->pluginResults = $results;
      }
    }
  }

  public function prepare() {
    $this->executePlugins();

    // Get rid of the query string if it's there
    $path = explode('?', $_SERVER['REQUEST_URI']);
    $path = array_shift($path);

    // Get our controller, action and any parameters passed on the url
    $vars = explode('/', ltrim($path, '/'));
    $this->controller = array_shift($vars) . 'Controller';
 
    // Load the default controller if Controller is empty
    if ('Controller' == $this->controller) {
      $this->controller = $this->configs['default.controller'] . $this->controller;
      $this->action = $this->configs['default.action'] . 'Action';
    } elseif (!stream_resolve_include_path($this->controller . '.php')) {
      $this->controller = $this->configs['error.controller'] . 'Controller';
      $this->action = $this->configs['error.action'] . 'Action';
    } else {
      $this->action = array_shift($vars) . 'Action';  
    }

    // Set action to index if empty
    if ('Action' == $this->action) {
      $this->action = 'index' . $this->action;
    }

    $this->variables = array();
    
    // Retrieve the passed in variables and put them in a local array
    while (count($vars)) {
      $this->variables[array_shift($vars)] = array_shift($vars);
    }

    // Post variables override all get and URI parameters
    foreach (array($_GET, $_POST) as $global) {
      foreach ($global as $key => $value) {
        $this->variables[$key] = $value;
      }
    }
  }

  public function execute() {
    $controller = $this->controller;
    $c = new $controller($this->variables, 
               $this->dbh, 
               new \Ferret\View($this->configs['layout']));

    if (isset($this->configs['plugins'][$this->controller])) {
      $c->view->setPlugins($this->configs['plugins'][$this->controller]);
    }
    
    if (is_array($this->pluginResults)) {
      $c->setPluginResults($this->pluginResults);
    }

    $action = $this->action;
    $c->$action();

    if ($c->getRender()) {
      $action = str_replace('Action', '', $this->action);
      echo $c->getOutput($action);
    }        
  }
}
