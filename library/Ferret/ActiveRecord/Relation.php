<?php

namespace Ferret\ActiveRecord;

abstract class Relation extends \Ferret\ActiveRecord {
  protected $ids = array();

  public function destroy($object) {

    $sql = "DELETE FROM {$this->tableName}
             WHERE {$this->ids[0]} = :{$this->ids[0]}
               AND {$this->ids[1]} = :{$this->ids[1]}";
    
    $query = $this->dbh->prepare($sql);

    $key = $this->ids[0];
    $query->bindParam($this->ids[0], $key);
    $key = $this->ids[1];
    $query->bindParam($this->ids[1], $key);
    
    return $query->execute();
  }

  public function save() {
    // First we remove the relation if it exists
    $this->destroy($this);

    // Now do our insert
    return $this->_insert();
  }

  public function findBy($field, $value) {
    return $this->_findBy(array($field), array($value));
  }
}