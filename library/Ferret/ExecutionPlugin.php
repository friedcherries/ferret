<?php

namespace Ferret;

abstract class ExecutionPlugin {
  protected $dbh;

  public function __construct($dbh) {
    $this->dbh = $dbh;
  }
  
  abstract function execute();
}
