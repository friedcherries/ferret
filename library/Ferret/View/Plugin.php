<?php

namespace Ferret\View;

abstract class Plugin {
  const PLUGIN_HEAD = 'head';
  const PLUGIN_BODY = 'body';
  const PLUGIN_FOOT = 'foot';
  public $location;

  abstract public function render();
}