<?php

namespace Ferret\View\Plugin;

class JavascriptLink extends \Ferret\View\Plugin {
  private $links;

  public function __construct() {
    $this->links = array();
    $this->location = \Ferret\View\Plugin::PLUGIN_FOOT;
  }

  public function render() {
    $content = '';
    foreach ($this->links as $url) {
        $content .= '<script src="' . $url . '"></script>';
    }

    return $content;
  }

  public function __constructor() {
    $this->links = array();
  }

  public function __set($name, $value) {
    $this->links[$name] = $value;
  }

  public function __get($name) {
    if (array_key_exists($name, $this->links)) {
        return $this->links[$name];
    } else {
        return null;
    }
  }

}