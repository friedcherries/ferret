<?php

namespace Ferret;

class Controller {
  public $view;
  protected $variables;
  protected $dbh;
  protected $render;
  protected $pluginResults;

  public function __construct($variables=array(), $dbh=null, $view=null) {
    if (null == $view) {
      $this->view = new \Ferret\View();
    } else {
      $this->view = $view;
    }

    $this->variables = $variables;
    $this->dbh = $dbh;
    $this->render = true;
  }

  public function getParam($name, $returnValue=null) {
    if (array_key_exists($name, $this->variables)) {
      return $this->variables[$name];
    } else {
      return $returnValue;
    }
  }

  public function getOutput($action) {
    $controller = get_class($this);
    $controller = str_replace('Controller', '', $controller);
    $this->view->set('controller', $controller);
    $this->view->set('action', $action);

    if (is_array($this->pluginResults)) {
      foreach ($this->pluginResults as $name => $value) {
        $this->view->set($name, $value);
      }
    }

    $content = $this->view->render($controller . DIRECTORY_SEPARATOR . strtolower($action)) ;

    if ($this->view->hasLayout()) {
      $this->view->set('content', $content);
      $content = $this->view->render('layout' . DIRECTORY_SEPARATOR . $this->view->getLayout());
    } else {
      error_log ('No layout');
    }

    return $content;
  }

  public function getRender() { return $this->render; }
  public function setRender($render) { $this->render = $render; }
  public function getPluginResults() { return $this->pluginResults; }
  public function setPluginResults($pluginResults) { $this->pluginResults = $pluginResults; }
}
