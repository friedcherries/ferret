<?php

namespace Ferret;

abstract class ActiveRecord {
  protected $dbh;
  protected $tableName;
  protected $className;
  protected $properties;
  protected $orderBy;

  public function __construct(\PDO $dbh) {
    $this->dbh = $dbh;
    $this->className = get_class($this);
    $this->_tableName();
    
    try {
      // Determine tablename from class and retrieve properties
      $sql = 'SELECT column_name FROM information_schema.columns WHERE LOWER(table_name)=LOWER(:table)';
      $query = $this->dbh->prepare($sql);
      $table = $this->tableName;
      $query->bindParam(':table', $table);
      $query->execute();
      $this->properties = array_fill_keys($query->fetchAll(\PDO::FETCH_COLUMN), null);

      // This shouldn't happen but just in case
      if (!is_array($this->properties)) { $this->properties = array(); }

    } catch (\PDOException $e) {
      error_log($e->getMessage());
      throw $e;
    }
  }

  public function toJson() {
    return json_encode($this->properties);
  }

  public function toArray() {
    return $this->properties;
  }

  public function all() {
    return $this->where();
  }

  public function find($id) {
    $found = $this->_findBy(array('id'), array($id));

    if (count($found) > 0) {
      return $found[0];
    } else {
      return null;
    }
  }

  public function save() {
    
    if ($this->properties['id'] != null) {
      $this->_update();
    } else {
      $this->_insert();
      $this->properties['id'] = $this->dbh->lastInsertId();
    }
  }

  public function destroy($object) {
    $sql = "DELETE FROM {$this->tableName} WHERE id = :id";
    $query = $this->dbh->prepare($sql);
    $id = $object->id;
    $query->bindParam(':id', $id);
    return $query->execute();
  }

  public function where($sqlPart='', $params=array()) {
    $sql = "SELECT * FROM {$this->tableName} WHERE 1=1 ";
    $sql .= $sqlPart . $this->orderBy;

    $query = $this->dbh->prepare($sql);

    foreach ($params as $key => $value) {
      $query->bindParam($key, $params[$key]);
    }

    $query->execute();
    return $this->_createObjects($query->fetchAll(\PDO::FETCH_ASSOC));
  }

  protected function _update() {
    $sql = "UPDATE {$this->tableName} SET ";
    
    foreach ($this->properties as $key => $value) {
      // Don't allow the dumbass to update the ID
      if ($key != 'id') {
        $sql .= " {$key} = :{$key},";
      }
    }
    
    $sql = trim($sql, ',');
    $sql .= ' WHERE id = :id';
    $query = $this->dbh->prepare($sql);

    foreach($this->properties as $key => $value) {
      // Don't let the dumbass to update the id
      if ($key != 'id') {
        $query->bindParam(":{$key}", $this->properties[$key]);
      }
    }

    // Now bind the ID because we need it for the where
    $query->bindParam(':id', $this->properties['id']);
    $value =  $query->execute();

    return $value;
  }

  protected function _insert() {
    $sql = "INSERT INTO {$this->tableName} (";
    foreach ($this->properties as $key => $value) {
      if ($key != 'id') {
        $sql .= " {$key},";
      }
    }

    $sql = trim($sql, ',');
    $sql .= ') VALUES (';

    foreach ($this->properties as $key => $value) {
      if ($key != 'id') {
        $sql .= " :{$key},";
      }
    }

    $sql = trim($sql, ',');
    $sql .= ')';

    $query = $this->dbh->prepare($sql);

    foreach ($this->properties as $key => $value) {
      if ($key != 'id') {
        $query->bindParam(":{$key}", $this->properties[$key]);
      }
    }

    $return = $query->execute();
    return $return;
  }

  protected function _tableName() {
    $classname = strtolower(get_class($this));
    $this->tableName = str_replace('\\', '_', $classname);
  }

  protected function _findBy($properties, $params) {
    $results = array();
    $sql = "SELECT * FROM {$this->tableName} WHERE 1=1";
    
    foreach ($properties as $field) {
      $sql .= " AND {$field} = :{$field}";
    }

    $sql .= $this->orderBy;
    
    $query = $this->dbh->prepare($sql);

    for ($x=0; $x<count($properties); $x++) {
      $query->bindParam(":{$properties[$x]}", $params[$x]);
    }

    $query->execute();
    $rows = $query->fetchAll(\PDO::FETCH_ASSOC);
    return $this->_createObjects($rows);
  }

  public function setOrderBy($fields) {
    $this->orderBy = ' ORDER BY ' . $fields;
  }

  protected function _createObjects($rows) {
    $results = array();
    
    if (is_array($rows)) {
      foreach ($rows as $row) {
        array_push($results, $this->_createObject($row));
      }
    }

    return $results;
  }

  protected function _createObject($row) {
    $cn = $this->className;
    $o = new $cn($this->dbh);
    $o->_setproperties($row);
    return $o;
  }

  protected function _setproperties($properties) {
    foreach ($properties as $key => $value) {
      $this->$key = $value;
    }
  }

  public function __get($name) {
    return (key_exists($name, $this->properties) ? $this->properties[$name] : null);
  }

  public function __set($name, $value) {
    if (key_exists($name, $this->properties) && in_array($name, $this->accessible)) {
      $this->properties[$name] = $value;
    }
  }

  public function __call($name, $params) {
    $properties = '';
    $results = null;
    if (strpos($name, 'findBy') !== false) {
      $function = str_replace('findBy', '', $name);
      $properties = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $function));
      $properties = explode('_and_', $properties);
      $results = $this->_findBy($properties, $params);
    }
    return $results;
  }
}
