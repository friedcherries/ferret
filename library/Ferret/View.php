<?php

namespace Ferret;

class View {
  protected $layout;
  protected $variables;
  protected $plugins;

  public function __construct($layout=null, $plugins=null) {
    $this->layout = $layout;
    $this->variables = array();
    $this->plugins = array('head' => array(),
      'body' => array(),
      'foot' => array());
    if (is_array($plugins)) {
      $this->setPlugins($plugins);
    }
  }

  public function renderPlugins($group) {
    $content = '';
    foreach ($this->plugins[$group] as $plugin) {
      $content = $plugin->render();
    }

    return $content;
  }

  public function render($template) {
    $content = '';
    if (stream_resolve_include_path($template . '.phtml')) {
      ob_start();
      require($template . '.phtml');
      $content = ob_get_clean();
    } else {
      error_log ('Error: ' . $template . ' not found!');
    }

    return $content;
  }

  public function &getPlugins($group) {
    return $this->plugins[$group];
  }

  public function setPlugins($plugins) {
    if (is_array($plugins)) {
      foreach ($plugins as $group => $plugs) {
        foreach ($plugs as $plugin => $class) {
          $this->loadPlugin($group, $plugin, $class);
        }
      }
    }
  }

  public function loadPlugin($group, $name, $class) {
    $this->plugins[$group][$name] = new $class();
  }

  public function hasLayout() {
    $layout = 'layout' . DIRECTORY_SEPARATOR . $this->layout;
    $status = stream_resolve_include_path($layout . '.phtml');
    return $status;
  }

  public function getLayout() {
    return $this->layout;
  }

  public function get($name) {
    return $this->variables[$name];
  }

  public function set($name, $value) {
    $this->variables[$name] = $value;
  }

  public function setVariables($variables) {
    $this->variables = $variables;
  }

  public function __get($name) {
    if (isset($this->variables[$name])) {
      return $this->variables[$name];
    } else {
      return null;
    }
  }

  public function __set($name, $value) {
    $this->variables[$name] = $value;
  }
}